# installer vue-cli

On fait référence à ce guide: https://vuejs.org/guide/quick-start.html

En prérequis, vous devez avoir installé nodejs (v16^) sur votre ordinateur.
Le projet créé utilisera une configuration de construction basée sur Vite et nous permettra d'utiliser les composants Vue Single-File (SFC).

> https://nodejs.org
> https://vitejs.dev/guide/

Vue CLI est un système complet pour le développement rapide de `Vue.js`, fournissant :

- Un échafaudage de projet interactif via `@vue/cli`.
- Une dépendance d'exécution (`@vue/cli-service`) qui est :
  - Évolutive ;
  - Construit au-dessus de webpack ou vite, avec des valeurs par défaut raisonnables ;
  - Configurable via un fichier de configuration dans le projet ;
  - Extensible via des plugins
- Une riche collection de plugins officiels intégrant les meilleurs outils de l'écosystème frontend.
- Une interface graphique complète pour créer et gérer les projets Vue.js.

```bash
  #install la cli de vue de manière globale
  npm install -g @vue/cli
  # OR
  yarn global add @vue/cli
  
  # OR
  npm init vue@latest
```

Voir la doc de vue-cli ici: https://cli.vuejs.org/

## Créer notre propre projet

```bash

# lancer l'assistant et choisir les options
# remplacer `my-project` par le nom de votre choix
vue create my-project


  # Vue.js - The Progressive JavaScript Framework
  # ✔ Add TypeScript? … No / Yes
  No
  # ✔ Add JSX Support? … No / Yes
  No
  # ✔ Add Vue Router for Single Page Application development? … No / Yes
  Yes
  # ✔ Add Pinia for state management? … No / Yes
  Yes
  # ✔ Add Vitest for Unit Testing? … No / Yes
  No
  # ✔ Add an End-to-End Testing Solution? › No
  No
  # ✔ Add ESLint for code quality? … No / Yes
  Yes
  # ✔ Add Prettier for code formatting? … No / Yes
  Yes

# aller dans le dossier du projet et exétuter l'application
cd my-project
npm run serve

```
