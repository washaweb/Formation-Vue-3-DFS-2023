//import du framework vue
import { createApp } from 'https://unpkg.com/vue@3/dist/vue.esm-browser.js'

createApp({
  data() {
    return {
      title: 'Hello World',
      year: new Date().getFullYear(),
    }
  }
}).mount('#app')