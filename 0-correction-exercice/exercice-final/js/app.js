//import du framework vue
import { createApp } from 'https://unpkg.com/vue@3/dist/vue.esm-browser.js'

createApp({
  data() {
    return {
      id: 1,
      imageSrc: '',
      billNumber: 1,
      date: '2023-03-22T09:35:16.975Z',
      name: 'Apple MacBook Air 13’ - occasion',
      description: 'Bon état, Couleur argent, 128 Go, clavier Azerty, Core i5 1,8Ghts, 8Go RAM',
      image: 'https://picsum.photos/200/300?image=1',
      client: {
        id: 134,
        firstName: 'John',
        lastName: 'Doe',
      },
      options: [
        {
          id: 1,
          name: 'clavier externe',
          priceHT: 49,
          image: 'https://picsum.photos/200/300?image=2',
        },
        {
          id: 2,
          name: 'Disque dur externe SSD, 512 Go',
          priceHT: 94,
          image: 'https://picsum.photos/200/300?image=3',
        }
      ],
      discountPercent: 10,
      paidAmount: 100,
      tva: true,
      priceHT: 450,
      tvaRate: 20,
      quantity: 1
    }
  },
  mounted() {
    // au chargement de l'application, on init
    // l'état de notre programme
    this.imageSrc = this.image
  },
  computed: {
    clientFullName(){
      return this.client.firstName + ' ' + this.client.lastName
    },
    dateFormated(){
      const d = new Date(this.date)
      return d.toLocaleDateString() + ' à ' + d.toLocaleTimeString()
    },
    // le prix promo a été mis à jour avec le prix des options
    // promoPriceHT() {
    //   return this.discountPercent > 0
    //     ? this.priceHT - (this.priceHT * this.discountPercent / 100)
    //     : this.priceHT
    // },
    promoPriceHT() {
      return this.discountPercent > 0
        ? this.fullPriceHT - (this.fullPriceHT * this.discountPercent / 100)
        : this.fullPriceHT
    },
    // computed qui sert à calculer le prix avec les options 
    // non remisé
    fullPriceHT(){
      let fullPrice = this.priceHT
      if(this.options.length) {
        for (const option of this.options) {
          fullPrice += option.priceHT
        }
      }
      return fullPrice
    },
    restDue(){
      return this.fullPriceHT - this.paidAmount
    },
    totalPanierHT(){
      return this.promoPriceHT * this.quantity
    },
    totalPanierTTC() {
      return this.totalPanierHT + (this.totalPanierHT * this.tva / 100)
    },
  },
  
  methods: {

  }
}).mount('#app')