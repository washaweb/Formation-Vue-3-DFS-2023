import { createApp } from 'vue'

createApp({
  data() {
    return {
      id: 1,
      billNumber: 1,
      date: '2023-03-22T09:35:16.975Z',
      name: 'Apple MacBook Air 13’ - occasion',
      description: 'Bon état, Couleur argent, <strong>128 Go</strong>, clavier Azerty, <em>Core i5 1,8Ghts, 8Go RAM</em>',
      client: {
        id: 134,
        firstName: 'John',
        lastName: 'Doe',
      },
      discountPercent: 10,
      paidAmount: 100,
      tva: true,
      priceHT: 450,
      tvaRate: 20
    }
  },
  computed: {
    dateFormated(){
      const d = new Date(this.date)
      return `${ d.toLocaleDateString()} à ${d.toLocaleTimeString()}`
    },
    restDue() {
      const restDue = this.priceHT - this.paidAmount
      return restDue.toFixed(2) + ' € HT'
    },
    clientFullName(){
      return this.client.firstName + ' ' + this.client.lastName
    }
  }
}).mount('#app')
