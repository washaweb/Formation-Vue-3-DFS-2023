import { createApp } from 'vue'

createApp({
  data() {
    return {
      imageSrc: '',
      id: 1,
      billNumber: 1,
      date: '2023-03-22T09:35:16.975Z',
      name: 'Apple MacBook Air 13’ - occasion',
      description: 'Bon état, Couleur argent, 128 Go, clavier Azerty, Core i5 1,8Ghts, 8Go RAM',
      image: 'https://picsum.photos/200/300?image=1',
      client: {
        id: 134,
        firstName: 'John',
        lastName: 'Doe',
      },
      options: [
        {
          id: 1,
          name: 'clavier externe',
          priceHT: 49,
          image: 'https://picsum.photos/200/300?image=2',
        },
        {
          id: 2,
          name: 'Disque dur externe SSD, 512 Go',
          priceHT: 94,
          image: 'https://picsum.photos/200/300?image=3',
        }
      ],
      discountPercent: 10,
      paidAmount: 100,
      tva: true,
      priceHT: 450,
      tvaRate: 20
    }
  },
  computed: {
    dateFormated(){
      const d = new Date(this.date)
      return `${ d.toLocaleDateString()} à ${d.toLocaleTimeString()}`
    },
    promoPriceHT(){
      // if(this.discountPercent>0) {
      //   return this.priceHT - (this.priceHT * this.discountPercent / 100)
      // }
      // return this.priceHT
      return this.discountPercent > 0 ? this.priceHT - (this.priceHT * this.discountPercent / 100) : this.priceHT
    },
    restDue() {
      const restDue = this.promoPriceHT - this.paidAmount
      return restDue.toFixed(2) + ' € HT'
    },
    clientFullName(){
      return this.client.firstName + ' ' + this.client.lastName
    },
  },
  mounted(){
    this.imageSrc = this.image
  },
  methods: {
    displayImage(src) {
      this.imageSrc = src
    }
  }
}).mount('#app')
