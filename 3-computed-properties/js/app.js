//import du framework vue
import { createApp } from 'https://unpkg.com/vue@3/dist/vue.esm-browser.js'

createApp({
  data() {
    return {
      author: {
        firstName: 'John',
        lastName: 'Doe',
        books: [
          'Vue 2 - Advanced Guide',
          'Vue 3 - Basic Guide',
          'Vue 4 - The Mystery to come',
        ]
      }
    }
  },
  // variables computed permet de rendre dynamique une méthode
  // par rapport à d'autres valeurs dynamiques (data, ou autre computed)
  computed: {
    publishedBooks(){
      return this.author.books.length > 0 ? 'Oui': 'Non'
    },
    fullName(){
      // return `${this.author.firstName} ${this.author.lastName}`
      return this.author.firstName + ' ' + this.author.lastName
    },
    // une computed peut faire appel à une autre computed
    reverseFullname() {
      return this.fullName.split('').reverse().join('')
    },
    now() {
      return new Date().toLocaleDateString()
    }
  },
  methods: {}
}).mount('#app')