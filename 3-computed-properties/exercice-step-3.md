# Exercice 2 - computed property

- Reprendre votre exercice précédent,
- ajouter l'affichage de la `date`, du prix Hors Taxe (`price`) de la facture et du montant déjà payé `paidAmount`
- créer une computed property `clientFullName` pour gérer l'affichage du nom complet du client (prénom + nom)
- créer des computed property `dateFormated` pour gérer l'affichage de la date avec un format `DD/MM/YYYY à hh:mm`
- créer une computed property `restDue` pour gérer l'affichage du montant restant dû (en se basant sur les valeurs de `paidAmount` et de `priceHT`)


```html
  <!-- code à ajouter dans la carte -->
	<div>Tarif HT: ... € HT - payé : ... € - <strong>Reste Dû : ...</strong></div>
  <div>Acheté le: ..., par <em>...</em></div>
  ```

```js
  {
    id: 1,
    billNumber: 1,
    date: '2023-03-22T09:35:16.975Z',
    name: 'Apple MacBook Air 13’ - occasion',
    description: 'Bon état, Couleur argent, 128 Go, clavier Azerty, Core i5 1,8Ghts, 8Go RAM',
    client: {
      id: 134,
      firstName: 'John',
      lastName: 'Doe',
    },
    discountPercent: 10,
    paidAmount: 100,
    tva: true,
    priceHT: 450,
    tvaRate: 20
  }
```
