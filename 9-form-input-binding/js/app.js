//import du framework vue
import { createApp } from 'https://unpkg.com/vue@3/dist/vue.esm-browser.js'

createApp({
  data() {
    return {
      count: 0,
      message: 'toto',
      debug: false,
      gender: null,
      selectedValue: null,
      options: [
        {
          text: 'Choisissez...',
          value: null
        },
        {
          text: 'JD',
          value: { nom: 'John', prenom: 'Doe'}
        },
        {
          text: 'JS',
          value: { nom: 'Jack', prenom: 'Smith'}
        },
        {
          text: 'MD',
          value: { nom: 'Marie', prenom: 'Dupont'}
        }
      ]
    }
  },
  methods: {
    onSubmit(){
      //verifications
      if(this.message.length<2){
        return
      }
      // envoi du message en ajax si tout est bon
      console.log(this.message)
    }
  },
}).mount('#app')