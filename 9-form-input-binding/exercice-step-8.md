# Exercice 8 - forms

- Reprendre l'exercice précédent
- Ajouter un champ input dans le template qui permet de contrôler une nouvelle valeur `quantity` pour gérer la quantité de produits
- la quantité ne peut pas être inférieure à 1 et suppérieure à 10
- Créer une computed qui calcule le prix totalPanierHT, et le prix totalPanierTTC en fonction de la quantité de produits saisie par l'utilisateur dans le champ ajouté
- Afficher ces valeurs dans le template sous l'artiche en utilisant le code suivant (en remplaçant les commentaires par votre code)

```html
<div class="row mt-2">
    <div class="col-6 col-lg-3">

      <label for="quantity" class="form-label mb-1">Quantité:</label>
      <div class="input-group mb-3">
        
        <!-- placer l'input ici -->

        <span class="input-group-text">unité(s)</span>
      </div>
    </div>
    <div class="col-6 col-lg-4 offset-lg-5">
      <ul class="list-group">
        <li class="list-group-item">total Panier HT: <strong>
          <!-- calculer le total HT ici --> 
          €
        </strong></li>
        <li class="list-group-item bg-primary-subtle">TOTAL Panier TTC: <strong>
          <!-- calculer le total TTC ici --> 
          €
        </strong></li>
      </ul>
    </div>
  </div>
</div>
```
