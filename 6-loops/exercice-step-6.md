# Exercice 6 - loops

- Reprendre l'exercice précédent, et modifier l'objet des data en complétant avec les données ci-dessous :
- En tenant compte de ces nouvelles données, dans le template, ajouter l'affichage de la liste des options en utilisant une directive de boucle `v-for`

```js
  {
    id: 1,
    billNumber: 1,
    date: '2023-03-22T09:35:16.975Z',
    name: 'Apple MacBook Air 13’ - occasion',
    description: 'Bon état, Couleur argent, 128 Go, clavier Azerty, Core i5 1,8Ghts, 8Go RAM',
    client: {
      id: 134,
      firstName: 'John',
      lastName: 'Doe',
    },

    options: [                  //<<------ new data start
      {
        id: 1,
        name: 'clavier externe',
        priceHT: 49,
      },
      {
        id: 2,
        name: 'Disque dur externe SSD, 512 Go',
        priceHT: 94,
      }
    ],                        //<<------ /new data end
    
    discountPercent: 10,
    paidAmount: 100,
    tva: true,
    priceHT: 450,
    tvaRate: 20
  }
```
