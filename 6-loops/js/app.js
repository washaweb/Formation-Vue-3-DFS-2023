//import du framework vue
import { createApp } from 'https://unpkg.com/vue@3/dist/vue.esm-browser.js'

createApp({
  data() {
    return {
      details: ['80% coton', '20% polyester', 'Genre mixte', 'rouge'],
      myObject: {
        title: 'How to do lists in Vue',
        author: 'Jane Doe',
        publishedAt: '2016-04-10'
      }
    }
  },
  computed: {},
  methods: {},
}).mount('#app')