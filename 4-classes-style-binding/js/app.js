//import du framework vue
import { createApp } from 'https://unpkg.com/vue@3/dist/vue.esm-browser.js'

createApp({
  data() {
    return {
      servers: [
        {
          id: 1,
          name: 'Prometheus',
          color: 'blue',
          background: '#fcff99',
          online: true
        },
        {
          id: 2,
          name: 'Pluton',
          color: '#00ff66',
          background: 'purple',
          online: true
        },
        {
          id: 3,
          name: 'Mars',
          color: 'black',
          background: 'pink',
          online: false
        }
      ]
    }
  },
  computed: {},
  methods: {},
}).mount('#app')