# Exercice 3 - classes and style binding

- Reprendre votre exercice précédent,

Ajouter ces styles et classes :

- Si facture est payée, elle doit s'afficher avec une bordure verte (utiliser les classes `border-3 border-success-subtle`)
- Si la facture n'est pas encore payée, elle doit s'afficher avec une bordure rouge (utiliser les classes `border-3 border-danger`).
- Tester en changeant la valeur de paidAmount à `450`, puis revenez à la valeur `100`