# Compoments

VueJs propose un système de composants qui permet de garder une structure de projet propre et efficace, applicant le principe DRY (Don't Repeat Yourself), nous pouvons alors répartir la logique de notre application dans plusieurs fichiers indépentants capable de communiquer entre eux et bénéficiant chacun de leur propre logique isolée du reste.
Les composants sont comme des fonctions réutilisable au niveau du template HTML.
Créer le code une fois, puis ré-exploitez le partout ou bon vous semble.

Un composant Vue doit être "enregistré" afin que Vue sache où localiser son implémentation lorsqu'il est rencontré dans un modèle. Il existe deux façons d'enregistrer les composants : globale et locale.

- import au niveau d'une vue ou d'un autre composant

```vue
  // exemple d'import d'un composant
  <script>
    import ComponentA from '@/components/ComponentA.vue'

    export default {
      components: {
        ComponentA
      }
    }
  </script>

  <template>
    <ComponentA />
  </template>
```

- import global à l'application

`src/main.js`

```js
  import MyComponent from '@/components/MyComponent.vue'
  //doit être placé après `const app = createApp(App)`
  app.component('MyComponent', MyComponent)
```

Voir la doc officielle ici :

> https://vuejs.org/guide/components/registration.html
