# Exercice component

Créer un composant button pour remplacer tous les boutons de notre application

Le composant bouton devra :

- utiliser 2 props `iconLeft` et `iconRight` pour permettre d'ajouter un icone à gauche ou à droite du texte
- utiliser 1 prop `variant` qui permettra de modifier la variante de style du bouton `primary, info, success, warning, danger, white, dark...`
- utiliser 1 prop `size` qui permettra de modifier la taille du bouton `sm, md, lg`

bonus : en vous basant sur la documentation de vue, déclarez l'import de votre nouveau composant `BButton` de manière globale dans votre application.
