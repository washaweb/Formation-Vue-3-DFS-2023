# Pourquoi utiliser un store ? et qu'est-ce qu'un store ?

Pinia est une bibliothèque de stockage (store) pour Vue, qui vous permet de partager un état entre tous les composants/pages.

Sans store, les données existent à l'intérieur d'un composant, mais sont isolées. Nous sommes obligés de faire communiquer les composants avec des `props` et des `$event` pour faire réagir un composant enfant ou un composant parent.
L'utilisation d'un store, permet d'extraire la logique du composant, et de la placer dans une "magazin de donnée", un store, externe au composant.
Celui-ci peut ainsi être appelé depuis n'importe où dans notre application, et ainsi maintenir un état commun à différents composants.

L'utilisation d'un store avec Pinia ou VueX est très utile :

- Support des outils de développement
- Une ligne de temps pour suivre les actions, les mutations
- Les stores apparaissent dans les composants où ils sont utilisés
- Voyage dans le temps et débogage plus facile
- Remplacement à chaud des modules
- Modifiez vos magasins sans recharger votre page
- Conservez tout état existant pendant le développement
- Plugins : étendre les fonctionnalités de Pinia avec des plugins
- Support TypeScript ou autocomplétion pour les utilisateurs JS
- Support du rendu côté serveur

## Créer un store

https://github.com/vuejs/pinia

Pour créer un store, il suffit de créer un nouveau fichier dans le dossier stores. Ce dossier existe car au moment de l'installation de notre projet, nous avons opté pour l'utilisation d'un store dans notre application.
Bien entendu il est toujours possible de l'ajouter après coup:

```bash
  yarn add pinia
  #OR
  npm i pinia
```

puis l'importer dans `src/main.js`

```js
  import { createApp } from 'vue'
  import { createPinia } from 'pinia' //<<<---ajout ici
  import App from './App.vue'

  const pinia = createPinia()  //<<<---ajout ici
  const app = createApp(App)

  app.use(pinia)
  app.mount('#app')
````



Créons un store exemple pour comprendre les basiques :

```js
  // stores/counter.js
  import { defineStore } from 'pinia'

  const useCounterStore = defineStore('counter', {
    state: () => ({ count: 0 }),
    getters: {
      double: (state) => state.count * 2,
    },
    actions: {
      increment() {
        this.count++
      },
    },
  })
```

## utiliser le store dans un composant

Pour ensuite pouvoir utiliser un store dans un ou plusieurs composants, il faut l'importer comme suit :

- on importe un ou plusieurs stores avec `...mapStores(store1, [[store2], store3, ...])`
- on importe l'état de chaque store avec qui représente les 'data' du store = `state` avec `...mapState(nomDuStore, ['var1', 'var2',...])`
- on importe les méthodes = `action` avec `...mapActions(nomDuStore, ['methode1', 'methode2'])`

```js
import { mapStores } from 'pinia'
import { useCounterStore } from '@/stores/counter'

export default defineComponent({
  computed: {
    // other computed properties
    // ...
    // gives access to this.counterStore and this.userStore
    ...mapStores(useCounterStore),
    // gives read access to this.count and this.double
    ...mapState(useCounterStore, ['count', 'double']),
  },
  methods: {
    // accéder à l'action du store this.increment()
    ...mapActions(useCounterStore, ['increment']),
  },
  mounted(){
    //exemple d'utilisation de la méthode au chargement du composant
    this.increment()
    console.log(this.count, this.double)
  }
})
```

## Créer notre propre store stores/bill.js

Grace à ce nouveau store, nous pouvons mettre à jour nos composants et bénéficier d'une donnée unique,
Qui sera appelée et mises à jour dans HomeView.vue et dans BillView.vue.


```js
// pour créer un store, on importe la méthode defineStore de pinia
import { defineStore } from 'pinia'

// création du store bill
const useBillStore = defineStore('bill', {
  // state = les datas du store
  state: () => ({
    bill: null, //nous servira à stocker le formulaire de la facture en cours de visualisation/edition dans EditBill
    bills: [] //liste des factures 
  }),

  // getters = computed du store
  getters: {
    totalBills: (state) => state.bills.length
  },

  // actions = méthodes du store
  actions: {

    // action d'ajout d'une facture
    createBill() {
      console.log('création d’une facture')
    },
    
    // action de modification d'une facture existante
    editBill(id) {
      console.log('suppression d’une facture', id)
    },
    
    deleteBill(id) {
      // pour faire appel au state ou aux computed, il suffit d'utiliser le mot clé `this`
      //this.bills fait donc référence au state.bills du store

      // retrouve l'index de la facture à supprimer dans le tableau
      const billToDeleteIndex = this.bills.findIndex((b) => b.id === id)
      // vérifier son code
      console.log('suppression d’une facture à l’index', billToDeleteIndex, 'avec l’id', id)
      // supprimer cette facture
      this.bills.splice(billToDeleteIndex, 1)
    }
  }
})
// export du store
export { useBillStore }

```


Pour aller plus loin:

- https://pinia.vuejs.org/
- https://blog.logrocket.com/complex-vue-3-state-management-pinia/
- https://stackblitz.com/edit/vue-3-pinia-registration-login-example
