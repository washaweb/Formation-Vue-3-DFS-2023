//import du framework vue
import { createApp } from 'https://unpkg.com/vue@3/dist/vue.esm-browser.js'

createApp({
  data() {
    return {
      count: 0,
    }
  },

  beforeCreate(){
    console.log('beforeCreate', this.count)
  },
  created(){
    this.count++
    console.log('created', this.count)
  },
  beforeMount(){
    this.count++
    console.log('beforeMount', this.count)
  },
  mounted() {
    this.count++
    console.log('mounted', this.count)
  },
  beforeUpdate() {
    // this.count++
    console.log('beforeUpdate', this.count)
  },
  updated() {
    // this.count++
    console.log('updated', this.count)
  },
  beforeUnmount(){
    this.count++
    console.log('beforeUnmount', this.count)
  },
  unmounted(){
    // this.count++
    console.log('unmounted', this.count)
    alert('done')
  },
  
  methods: {
    onAddCount(){
      this.count++
    },
    onMinusCount() {
      if(this.count <= 0) {
        alert('Le compteur est à 0')
        return
      }
      this.count--
    },
  },
}).mount('#app')