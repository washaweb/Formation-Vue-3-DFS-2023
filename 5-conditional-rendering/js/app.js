//import du framework vue
import { createApp } from 'https://unpkg.com/vue@3/dist/vue.esm-browser.js'

createApp({
  data() {
    return {
      title: 'Hello World', //des données de l'application
      year: new Date().getFullYear(),
      active: true,
      product: {
        name: 'Chaussette',
        image: 'https://picsum.photos/id/1/200/300',
        stock: 100,
        rating: 2,
        details: ['80% coton', '20% polyester', 'Genre mixte', 'rouge']
      }
    }
  },
  computed: {},
  methods: {},
}).mount('#app')