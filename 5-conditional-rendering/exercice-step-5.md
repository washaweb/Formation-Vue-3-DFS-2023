# Exercice 5 - conditionnal rendering

- Reprendre l'exercice précédent, et ajouter cette condition d'affichage :
- Si l'article est en promotion, barrer le prix normal, et afficher le nouveau prix remisé à côté du prix barré
- modifier la computed property `restDue` pour qu'elle tienne compte du prix remisé
