//import du framework vue
import { createApp } from 'https://unpkg.com/vue@3/dist/vue.esm-browser.js'

createApp({
  data() {
    return {
      count: 1,
      name: 'Lux Skywalker',
      online: true,
      monJedi: {
        nom: 'Skywalker',
        prenom: 'Luc',
        jedi: true,
        skills: ['Sabre laser', 'La force']
      },
    }
  },
  methods: {
    updateCount(){
      this.count++
    }
  }
}).mount('#app')