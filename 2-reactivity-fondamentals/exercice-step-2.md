# Exercice 1 - template syntax

- Reprendre l'exercice précédent, et ajouter les variables dynamiques provenant de l'objet suivant :
- Le template HTML doit maintenant afficher la valeur des champs suivants: 
  - `name`
  - `description` au format HTML
  - le `firstname` et `lastname` du client

```js
  {
    id: 1,
    billNumber: 1,
    date: '2023-03-22T09:35:16.975Z',
    name: 'Apple MacBook Air 13’ - occasion',
    description: 'Bon état, Couleur argent, 128 Go, clavier Azerty, Core i5 1,8Ghts, 8Go RAM',
    client: {
      id: 134,
      firstName: 'John',
      lastName: 'Doe',
    },
    discountPercent: 10,
    paidAmount: 100,
    tva: true,
    priceHT: 450,
    tvaRate: 20
  }
```
