# Exercice 7 - events

- Reprendre l'exercice précédent, et réorganiser le template pour afficher une image comme ceci:
- modifier l'objet data en complétant avec les données ci-dessous
- Intégrer cet algorithme d'événements :
  - par défaut l'image affichée est celle du produit
  - au survol de la souris sur l'une des options, l'image du produit change et affiche l'image de l'option survollée
  - à la sortie du surbol, on affiche à nouveau l'image du produit

```html
  <article class="card shadow border-2">
    <div class="d-flex align-items-start">
      <img class="img-fluid rounded-start" src="..." />
      <div class="card-body">
        // contenu de la carte ici
      </div>
    </div>
  </article>
```

```js
  {
    id: 1,
    billNumber: 1,
    date: '2023-03-22T09:35:16.975Z',
    name: 'Apple MacBook Air 13’ - occasion',
    description: 'Bon état, Couleur argent, 128 Go, clavier Azerty, Core i5 1,8Ghts, 8Go RAM',

    image: 'https://picsum.photos/200/300?image=1',                                    // <---- new
    
    client: {
      id: 134,
      firstName: 'John',
      lastName: 'Doe',
    },

    options: [
      {
        id: 1,
        name: 'clavier externe',
        priceHT: 49,
        image: 'https://picsum.photos/200/300?image=2',                                // <---- new
      },
      {
        id: 2,
        name: 'Disque dur externe SSD, 512 Go',
        priceHT: 94,
        image: 'https://picsum.photos/200/300?image=3',                                // <---- new
      }
    ],
    discountPercent: 10,
    paidAmount: 100,
    tva: true,
    priceHT: 450,
    tvaRate: 20
  }
```
