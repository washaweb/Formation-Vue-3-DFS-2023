//import du framework vue
import { createApp } from 'https://unpkg.com/vue@3/dist/vue.esm-browser.js'

createApp({
  data() {
    return {
      count: 0,
      selectedId: 2234,
      product: {
        image: 'https://picsum.photos/200/300?image=1',
        variants: [
          {
            id: 2234,
            variantColor: 'vert',
            variantImage: 'https://picsum.photos/200/300?image=1'
        },
        {
            id: 2235,
            variantColor: 'bleu',
            variantImage: 'https://picsum.photos/200/300?image=2'
        }
        ]
      }
    }
  },
  
  methods: {
    onAddCount(){
      this.count++
    },
    onMinusCount() {
      if(this.count <= 0) {
        alert('Le compteur est à 0')
        return
      }
      this.count--
    },
    // je récupère la variante depuis le template
    // au moment ou l'utilisateur survolle l'image variante
    onUpdateImageVariant(variantImage, id) {
      console.log(id)
      // je place la nouvelle source
      // dans la grande image du produit
      this.product.image = variantImage
      this.selectedId = id
    }
  },
}).mount('#app')